package com.cms.backend.master.service;

import com.cms.backend.master.exception.AppException;
import com.cms.backend.master.model.Product;
import com.cms.backend.master.repository.ProductRepository;
import com.cms.backend.master.response.ErrorResponse;
import com.cms.backend.master.response.ProductsResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Product service
 *
 * @author NhatVH
 * @version 1.0
 * @since 2020-02-19
 */
@Service
@Slf4j
public class ProductService {
    private ProductRepository productRepository;
    private static final AppException NOT_FOUND_PRODUCT = new AppException(new ErrorResponse("Not found product"), HttpStatus.NOT_FOUND);

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    /**
     * List all products
     *
     * @return all products
     */
    public ProductsResponse listProduct() {
        return ProductsResponse.builder()
                .products(productRepository.findAll())
                .build();
    }

    /**
     * Find a product
     *
     * @return product
     */
    public ResponseEntity<Product> getProduct(String id) throws AppException {

        Optional<Product> optionalProduct = productRepository.findById(id);
        if (optionalProduct.isPresent()) {
            return new ResponseEntity<>(optionalProduct.get(), HttpStatus.OK);
        } else {
            log.info("# not found product {}", id);
            throw NOT_FOUND_PRODUCT;
        }
    }

    /**
     * save a product
     *
     * @return saved product
     */
    public Product saveProduct(Product product) {
        return productRepository.save(product);
    }

    /**
     * Update products
     *
     * @return updated product
     */
    public ResponseEntity<Product> updateProduct(String id, Product inputProduct) throws AppException {
        Optional<Product> optionalProduct = productRepository.findById(id);
        if (optionalProduct.isPresent()) {
            Product oldProduct = optionalProduct.get();
            oldProduct.setName(inputProduct.getName());
            oldProduct.setDescription(inputProduct.getDescription());
            Product productUpdated = productRepository.save(oldProduct);
            log.info("# update product {}", id);
            return new ResponseEntity<>(productUpdated, HttpStatus.OK);
        } else {
            log.info("# not found product {}", id);
            throw NOT_FOUND_PRODUCT;
        }
    }

    /**
     * delete product
     */
    public void deleteProduct(String id) throws AppException {
        if (productRepository.existsById(id)) {
            productRepository.deleteById(id);
            log.info("# delete product {}", id);
        } else {
            log.info("# not found product {}", id);
            throw NOT_FOUND_PRODUCT;
        }
    }
}
