package com.cms.backend.master.repository;

import com.cms.backend.master.model.Product;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Product repository
 *
 * @author NhatVH
 * @version 1.0
 * @since 2020-02-19
 */
@Repository
public interface ProductRepository extends MongoRepository<Product, String> {
}
