package com.cms.backend.master.controller;

import com.cms.backend.master.exception.AppException;
import com.cms.backend.master.model.Product;
import com.cms.backend.master.response.ProductsResponse;
import com.cms.backend.master.service.ProductService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Product controller
 *
 * @author NhatVH
 * @version 1.0
 * @since 2020-02-19
 */
@RestController
@Slf4j
public class ProductController {

    // Services
    // -------------------------------------------------------------------------
    private ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    /**
     * List all products
     *
     * @return all products
     */
    @ApiOperation(value = "List all products")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The resource has been fetched success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 403, message = "access to this resource on the server is denied"),
            @ApiResponse(code = 500, message = "server cannot process the request")
    })
    @GetMapping(path = "/products")
    public ResponseEntity<ProductsResponse> listProduct() {
        log.info("# list all");
        return new ResponseEntity<>(productService.listProduct(), HttpStatus.OK);
    }

    /**
     * Find a product
     *
     * @return product
     */
    @ApiOperation(value = "List all products")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The resource has been fetched success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 403, message = "access to this resource on the server is denied"),
            @ApiResponse(code = 500, message = "server cannot process the request")
    })
    @GetMapping(path = "/products/{id}")
    public ResponseEntity<Product> getProduct(@PathVariable("id") String id) throws AppException {
        log.info("# view product {}", id);
        return productService.getProduct(id);
    }

    /**
     * save a product
     *
     * @return saved product
     */
    @ApiOperation(value = "List all products")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The resource has been fetched success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 403, message = "access to this resource on the server is denied"),
            @ApiResponse(code = 500, message = "server cannot process the request")
    })
    @PostMapping(path = "/products")
    public ResponseEntity<Product> saveProduct(@RequestBody Product product) {
        log.info("# save product");
        return new ResponseEntity<>(productService.saveProduct(product), HttpStatus.CREATED);
    }

    /**
     * Update products
     *
     * @return updated product
     */
    @ApiOperation(value = "update product")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The resource has been fetched success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 403, message = "access to this resource on the server is denied"),
            @ApiResponse(code = 500, message = "server cannot process the request")
    })
    @PutMapping(path = "/products/{id}")
    public ResponseEntity<Product> updateProduct(@PathVariable String id, @RequestBody Product product) throws AppException {
        log.info("# save product");
        return productService.updateProduct(id, product);
    }

    /**
     * delete product
     */
    @ApiOperation(value = "delete product")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The resource has been fetched success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 403, message = "access to this resource on the server is denied"),
            @ApiResponse(code = 500, message = "server cannot process the request")
    })
    @DeleteMapping(path = "/products/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable String id) throws AppException {
        log.info("# delete product");
        productService.deleteProduct(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
