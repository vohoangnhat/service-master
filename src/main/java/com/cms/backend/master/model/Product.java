package com.cms.backend.master.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Product Document
 *
 * @author NhatVH
 * @version 1.0
 * @since 2020-02-19
 */
@Data
@Document(collection = "products")
public class Product {
    @Id
    @ApiModelProperty(value = "id of product")
    private String id;
    // name of product
    @ApiModelProperty(value = "name of product")
    private String name;
    // description of product
    @ApiModelProperty(value = "description of product")
    private String description;
}
