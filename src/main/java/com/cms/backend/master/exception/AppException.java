package com.cms.backend.master.exception;

import com.cms.backend.master.response.ErrorResponse;
import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * App exception
 *
 * @author NhatVH
 * @version 1.0
 * @since 2020-02-19
 */
@Getter
public class AppException extends Throwable {

    private HttpStatus httpStatus;
    private ErrorResponse response;

    public AppException(ErrorResponse response, HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
        this.response = response;
    }
}
