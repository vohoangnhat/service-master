package com.cms.backend.master.response;

import com.cms.backend.master.model.Product;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * Product controller
 *
 * @author NhatVH
 * @version 1.0
 * @since 2020-02-19
 */
@Data
@Builder
public class ProductsResponse {
    private List<Product> products;
}
