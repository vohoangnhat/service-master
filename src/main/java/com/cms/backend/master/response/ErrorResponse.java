package com.cms.backend.master.response;

import lombok.Data;

/**
 * Error response
 *
 * @author NhatVH
 * @version 1.0
 * @since 2020-02-19
 */
@Data
public class ErrorResponse {

    private String message;

    public ErrorResponse(String message) {
        this.message = message;
    }
}
