package com.cms.backend.master.handler;

import com.cms.backend.master.exception.AppException;
import com.cms.backend.master.response.ErrorResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Exception handler
 *
 * @author NhatVH
 * @version 1.0
 * @since 2020-02-19
 */
@ControllerAdvice
public class GlobalExceptionHandler {


    @ExceptionHandler(value = {AppException.class})
    public ResponseEntity<ErrorResponse> error(AppException e) {
        return new ResponseEntity<>(e.getResponse(), e.getHttpStatus());

    }
}
